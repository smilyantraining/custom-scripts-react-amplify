describe('My First Test', function () {
    it('Finds the login form & login with wrong user', function () {
        cy.visit('http://localhost:3000/')

        cy.get('[name="username"]')
            .type('smilyan.pavlov')
            .should('have.value', 'smilyan.pavlov')
        
        cy.get('[name="password"]')
            .type('Password12@')
            .should('have.value', 'Password12@')

        cy.get('button')
            .click()

        cy.get('.Toast__toast___2UNlJ span').should('have.text', 'User does not exist');
    })
});
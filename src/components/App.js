import React, { Component } from 'react';
import { BrowserRouter, Switch, Redirect } from 'react-router-dom';
import { observer } from 'mobx-react';

import UserStore from '../stores/UserStore';

import Layout from './layout/Layout';

import Amplify from 'aws-amplify';
import aws_exports from '../aws-exports';
import { withAuthenticator } from 'aws-amplify-react';
Amplify.configure(aws_exports);


@observer

class App extends Component {
  state = {
    userGroups: []
  }
  
  componentDidMount() {
    UserStore.authorize();
  }

  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Redirect exact from='/' to='/available/map' />
          <Redirect exact from='/logout' to='/' />
          <Layout props={this.props} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default withAuthenticator(App, false);

import React from 'react';
import { Link } from 'react-router-dom';

const SidebarNavigation = (props) => {
    const page = props.match.params.page;
    return (
        <nav>
            <ul>
                <li><Link to={`/available/${page}`}>Available</Link></li>
                <li><Link to={`/accepted/${page}`}>Accepted</Link></li>
                <li><Link to={`/arrived/${page}`}>Arrived</Link></li>
            </ul>
        </nav>
    )
}

export default SidebarNavigation;
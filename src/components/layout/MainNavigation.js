import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { observer } from 'mobx-react';

import UserStore from '../../stores/UserStore';

@observer

class MainNavigation extends Component {
    render() {
        const tab = this.props.match.params.tab;
        return (
            <nav>
                <ul>
                    <li><Link to={`/${tab}/add-job`}>Add a job</Link></li>
                    <li><Link to={`/${tab}/reporting`}>Reporting</Link></li>
                    <li><Link to={`/${tab}/manage-users`}>Manage Users</Link></li>
                    <li><Link to="/logout" onClick={() => UserStore.logOut()}>Logout</Link></li>
                </ul>
            </nav>
        )
    }
}

export default MainNavigation;
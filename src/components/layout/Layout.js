import React, { Component } from 'react'
import { Route } from 'react-router-dom';
import { observer } from 'mobx-react';

import { isUserInGroup } from '../../utils/permissions';

import AvailableJobs from '../jobs/AvailableJobs';
import AcceptedJobs from '../jobs/AcceptedJobs';
import ArrivedJobs from '../jobs/ArrivedJobs';
import AddJob from '../jobs/AddJob';

import Map from '../pages/Map';
import Reporting from '../pages/Reporting';
import ManageUsers from '../pages/ManageUsers';

import SidebarNavigation from './SidebarNavigation';
import MainNavigation from './MainNavigation';

import UserStore from '../../stores/UserStore';

@observer

class Layout extends Component {

  render() {
    let userGroups;
    if (UserStore.user.userGroups) {
      userGroups = UserStore.user.userGroups.join(', ');
      console.log(userGroups);
    }

    if (isUserInGroup('controller') || isUserInGroup('hcp') || isUserInGroup('gp')) {
      return (
        <div className="App">
          <div className="sidebar">
            <div className="sidebar-tabs">
              <Route path="/:tab/:page" component={SidebarNavigation} />
            </div>
            <div className="sidebar-content">
              <Route path="/available/:page" component={AvailableJobs} />
              <Route path="/accepted/:page" component={AcceptedJobs} />
              <Route path="/arrived/:page" component={ArrivedJobs} />
            </div>
          </div>
          <div className="main">
            <div className="main-navigation">
              <Route
                path='/:tab/:page'
                render={(props) => <MainNavigation {...props} />}
              />
            </div>
            <div className="main-content">
              <h1>This layout is for any user that <em>IS</em> a Controller, HCP, GP. User is {userGroups}</h1>
              <Route path="/:tab/map" component={Map} />
              <Route path="/:tab/add-job" component={AddJob} />
              <Route path="/:tab/reporting" component={Reporting} />
              <Route path="/:tab/manage-users" component={ManageUsers} />
            </div>
          </div>
        </div>
      )
    } else {
      return (
        <div className="App">
          <h1>This content is for any user that is <em>NOT</em> a Controller, HCP, GP. User is {userGroups}</h1>
          <p><button onClick={() => UserStore.logOut()}>Logout</button></p>
        </div>
      )
    }
  }
}

export default Layout;
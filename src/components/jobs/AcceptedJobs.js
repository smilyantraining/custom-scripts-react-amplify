import React, { Component } from 'react'
import JobsList from './JobsList';

class AcceptedJobs extends Component {
  render() {
    return (
      <div>
        <h2>Accepted Jobs</h2>
        <JobsList />
      </div>
    )
  }
}

export default AcceptedJobs;

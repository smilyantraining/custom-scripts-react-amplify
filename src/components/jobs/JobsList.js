import React, { Component } from 'react'
import JobListItem from './JobListItem';

class JobsList extends Component {
  render() {
    return (
      <div>
        <ul>
            <li><JobListItem /></li>
            <li><JobListItem /></li>
            <li><JobListItem /></li>
            <li><JobListItem /></li>
            <li><JobListItem /></li>
        </ul>
      </div>
    )
  }
}

export default JobsList;
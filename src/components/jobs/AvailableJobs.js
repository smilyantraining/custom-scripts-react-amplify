import React, { Component } from 'react'
import JobsList from './JobsList';

class AvailableJobs extends Component {
  render() {
    return (
      <div>
        <h2>Available Jobs</h2>
        <JobsList />
      </div>
    )
  }
}

export default AvailableJobs;
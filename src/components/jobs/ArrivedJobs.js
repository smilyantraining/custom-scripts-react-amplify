import React, { Component } from 'react'
import JobsList from './JobsList';

class ArrivedJobs extends Component {
  render() {
    return (
      <div>
        <h2>Arrived Jobs</h2>
        <JobsList />
      </div>
    )
  }
}

export default ArrivedJobs;
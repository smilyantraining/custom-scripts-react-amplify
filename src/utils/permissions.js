import UserStore from '../stores/UserStore';

export const isUserInGroup = (group) => {
    return UserStore.user.userGroups && UserStore.user.userGroups.filter(userGroup => userGroup === group).length;
}
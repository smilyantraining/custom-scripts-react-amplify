import React from 'react';
import ReactDOM from 'react-dom';
import './assets/index.scss';
import App from './components/App';

import { Provider } from 'mobx-react';
import * as stores from './stores/index';

const Root = (
    <Provider {...stores}>
        <App />
    </Provider>
);

ReactDOM.render(Root, document.getElementById('root'));
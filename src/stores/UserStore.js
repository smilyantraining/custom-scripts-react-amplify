import { observable, action } from 'mobx';
import { Auth } from 'aws-amplify';

class UserStore {

    @observable user = {};
    @observable isLoading = true;

    @action updateUser = (user) => {
        this.user = user;
    }

    @action authorize = async () => {
        const store = this;
        const userSession = await Auth.currentSession();
        console.log('userSession: ', userSession);
        store.updateUser({
            tokens: {
                access: userSession.accessToken.jwtToken,
                id: userSession.idToken.jwtToken,
                refresh: userSession.refreshToken.token,
            },
            userGroups: userSession.idToken.payload['cognito:groups'],
            username: userSession.idToken.payload['cognito:username'],
            email: userSession.idToken.payload.email
        });

        this.isLoading = false;
    }

    @action logOut = async () => {
        try {
            await Auth.signOut();
            window.location.reload();
        } catch (error) {
            console.log(error);
        }
    }
}

export default new UserStore();